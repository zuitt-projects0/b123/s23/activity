db.users.insertMany([
	{
		"firstName":"Brendon",
		"lastName":"Urie",
		"email":"panic!atTheDisco@gmail.com",
		"password":"passpanic!!!",
		"isAdmin":false
	},
	{
		"firstName":"Adam",
		"lastName":"Levine",
		"email":"maroonPayb@gmail.com",
		"password":"FivemaRoon",
		"isAdmin":false
	},
	{
		"firstName":"Lisa",
		"lastName":"Orebe",
		"email":"LiSa@gmail.com",
		"password":"gurenge",
		"isAdmin":false
	},
	{
		"firstName":"Ben",
		"lastName":"Ben",
		"email":"B&B@gmail.com",
		"password":"bibingka",
		"isAdmin":false
	},
	{
		"firstName":"Zack",
		"lastName":"Tabudlo",
		"email":"habangBuhay@gmail.com",
		"password":"foreverLife",
		"isAdmin":false
	}
])

db.courses.insertMany([
	{
		"name":"Programming 101",
		"description":"Teaches the fundamentals of programming",
		"price": 5000,
		"isActive":true
	},
	{
		"name":"HCI",
		"description":"Teaches the human and computer interaction",
		"price": 3000,
		"isActive":true
	},
	{
		"name":"Data Structures & Algorithms",
		"description":"Teaches the fundamentals of data structures and algorithms",
		"price": 6000,
		"isActive":true
	}
])

db.users.updateOne({"email":"LiSa@gmail.com"},{$set:{"isAdmin":true}})

db.users.find({"isAdmin":false})

db.courses.updateOne({"name":"HCI"},{$set:{"isActive":false}})
